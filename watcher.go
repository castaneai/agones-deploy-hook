package agones_deploy_hook

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	agonesv1 "agones.dev/agones/pkg/apis/agones/v1"
	"agones.dev/agones/pkg/client/clientset/versioned"
	"agones.dev/agones/pkg/client/informers/externalversions"
	"k8s.io/client-go/tools/cache"
)

const (
	defaultResync = 3 * time.Second
)

type FleetDeployEventType string

const (
	FleetDeployEventTypeStart  FleetDeployEventType = "Start"
	FleetDeployEventTypeFinish FleetDeployEventType = "Finish"
	FleetDeployEventTypeError  FleetDeployEventType = "Error"
)

// FleetDeployEventPayload is the body of the webhook request
type FleetDeployEventPayload struct {
	// FleetName is the name of the updated Fleet
	FleetName string `json:"fleetName"`
	// Namespace is the namespace of the updated Fleet
	Namespace string `json:"namespace"`
	// EventType is the type of event
	EventType FleetDeployEventType `json:"fleetEventType"`
	// The updated Fleet
	Fleet agonesv1.Fleet `json:"fleet"`
}

type Watcher struct {
	agonesClient  versioned.Interface
	namespace     string
	webhookClient *http.Client
	webhookURL    string
}

func NewWatcher(agonesClient versioned.Interface, namespace, webhookURL string) *Watcher {
	return &Watcher{
		agonesClient:  agonesClient,
		namespace:     namespace,
		webhookClient: http.DefaultClient,
		webhookURL:    webhookURL,
	}
}

func (w *Watcher) Start(ctx context.Context) {
	deploying := map[string]struct{}{}
	factory := externalversions.NewSharedInformerFactoryWithOptions(w.agonesClient, defaultResync,
		externalversions.WithNamespace(w.namespace))
	informer := factory.Agones().V1().Fleets()
	informer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			flt := obj.(*agonesv1.Fleet)
			if _, ok := deploying[flt.Name]; ok {
				return
			}
			deploying[flt.Name] = struct{}{}
			if err := w.callWebhook(ctx, FleetDeployEventTypeStart, flt); err != nil {
				log.Printf("failed to call webhook: %+v", err)
			}
		},
		UpdateFunc: func(oldObj, newObj interface{}) {
			flt := newObj.(*agonesv1.Fleet)
			// spec.Replicas == 0 のfleetは使われないFleetとみなし通知をしない
			if flt.Spec.Replicas == 0 {
				return
			}
			if _, ok := deploying[flt.Name]; ok {
				if deployFinished(flt) {
					if err := w.callWebhook(ctx, FleetDeployEventTypeFinish, flt); err != nil {
						log.Printf("failed to call webhook: %+v", err)
					}
					delete(deploying, flt.Name)
				}
			}
		},
		DeleteFunc: func(obj interface{}) {
			flt := obj.(*agonesv1.Fleet)
			delete(deploying, flt.Name)
		},
	})
	factory.Start(ctx.Done())
}

func (w *Watcher) callWebhook(ctx context.Context, eventType FleetDeployEventType, flt *agonesv1.Fleet) error {
	payload := &FleetDeployEventPayload{
		FleetName: flt.Name,
		Namespace: flt.Namespace,
		EventType: eventType,
		Fleet:     *flt,
	}
	reqBody, err := json.Marshal(payload)
	if err != nil {
		return fmt.Errorf("failed to marshal %T to json: %w", payload, err)
	}
	req, err := http.NewRequestWithContext(ctx, "POST", w.webhookURL, bytes.NewReader(reqBody))
	if err != nil {
		return fmt.Errorf("failed to create request: %w", err)
	}
	resp, err := w.webhookClient.Do(req)
	if err != nil {
		return fmt.Errorf("failed to post webhook to %s: %w", w.webhookURL, err)
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			log.Printf("failed to close http response body: %+v", err)
		}
	}()
	if resp.StatusCode >= 400 {
		var errs string
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			errs = fmt.Sprintf("<failed to read response body: %+v>", err)
		} else {
			errs = string(b)
		}
		return fmt.Errorf("bad status code %d %s from the server: %s", resp.StatusCode, resp.Status, errs)
	}
	return nil
}

func deployFinished(flt *agonesv1.Fleet) bool {
	return flt.Status.ReadyReplicas+flt.Status.AllocatedReplicas+flt.Status.ReservedReplicas >= flt.Spec.Replicas
}
