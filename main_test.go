package agones_deploy_hook

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"

	agonesv1 "agones.dev/agones/pkg/apis/agones/v1"
	autoscalingv1 "agones.dev/agones/pkg/apis/autoscaling/v1"
	e2eframework "agones.dev/agones/test/e2e/framework"
	"github.com/google/uuid"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	namespacePrefix = "gameserver-test-"
)

var framework *e2eframework.Framework

func TestMain(m *testing.M) {
	fw, err := e2eframework.NewFromFlags()
	if err != nil {
		log.Fatalf("failed to init e2e e2eframework: %+v", err)
	}
	framework = fw

	var exitCode int
	defer func() {
		os.Exit(exitCode)
	}()
	exitCode = m.Run()
}

func newFleetResource(name, namespace string, replicas int, gstSpec *agonesv1.GameServerTemplateSpec) *agonesv1.Fleet {
	return &agonesv1.Fleet{
		ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace},
		Spec: agonesv1.FleetSpec{
			Replicas: int32(replicas),
			Template: *gstSpec,
		},
	}
}

func newFleet(t *testing.T, name, namespace string, replicas int, gstSpec *agonesv1.GameServerTemplateSpec) *agonesv1.Fleet {
	flt, err := framework.AgonesClient.AgonesV1().
		Fleets(namespace).
		Create(context.Background(), newFleetResource(name, namespace, replicas, gstSpec), metav1.CreateOptions{})
	if err != nil {
		t.Fatalf("failed to create fleet: %+v", err)
	}
	return flt
}

func newGameServerTemplateSpec(name, image string) *agonesv1.GameServerTemplateSpec {
	return &agonesv1.GameServerTemplateSpec{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: agonesv1.GameServerSpec{
			Ports: []agonesv1.GameServerPort{
				{
					ContainerPort: 7000,
					Name:          "tcp",
					PortPolicy:    agonesv1.Dynamic,
					Protocol:      corev1.ProtocolTCP,
				}},
			Template: corev1.PodTemplateSpec{
				Spec: newPodSpec(name, image),
			},
		},
	}
}

func newPodSpec(name, image string) corev1.PodSpec {
	return corev1.PodSpec{
		Containers: []corev1.Container{{
			Name:            name,
			Image:           image,
			ImagePullPolicy: corev1.PullIfNotPresent, // prefer local image
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("30m"),
					corev1.ResourceMemory: resource.MustParse("32Mi"),
				},
				Limits: corev1.ResourceList{
					corev1.ResourceCPU:    resource.MustParse("30m"),
					corev1.ResourceMemory: resource.MustParse("32Mi"),
				},
			},
		}},
	}
}

func newRandomNamespace(t *testing.T) string {
	namespace := fmt.Sprintf("%s%s", namespacePrefix, uuid.Must(uuid.NewRandom()))
	if err := framework.CreateNamespace(namespace); err != nil {
		panic(err)
	}
	t.Cleanup(func() {
		_ = framework.DeleteNamespace(namespace)
	})
	return namespace
}

func newFleetAutoscaler(t *testing.T, namespace, name string, spec *autoscalingv1.FleetAutoscalerSpec) {
	if _, err := framework.AgonesClient.AutoscalingV1().FleetAutoscalers(namespace).
		Create(context.Background(), &autoscalingv1.FleetAutoscaler{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: *spec,
		}, metav1.CreateOptions{}); err != nil {
		t.Fatalf("failed to create fleet autoscaler: %+v", err)
	}
}
