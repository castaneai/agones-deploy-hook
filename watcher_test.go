package agones_deploy_hook

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	agonesv1 "agones.dev/agones/pkg/apis/agones/v1"
	allocationv1 "agones.dev/agones/pkg/apis/allocation/v1"
	autoscalingv1 "agones.dev/agones/pkg/apis/autoscaling/v1"
	"agones.dev/agones/pkg/client/informers/externalversions"
	"github.com/stretchr/testify/assert"
	eventsv1 "k8s.io/api/events/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/tools/cache"
)

const (
	imageOld              = "gcr.io/agones-images/simple-game-server:0.5"
	imageNew              = "gcr.io/agones-images/simple-game-server:0.6"
	allocateRevisionLabel = "allocate-revision"
)

func TestDeployTwoFleets(t *testing.T) {
	ns := newRandomNamespace(t)

	deployEvents := make(chan *FleetDeployEventPayload)
	revisionPromoted := make(chan string)
	go allocateLoop(t, ns, revisionPromoted)

	webhookReceiver := startWebhookReceiver(deployEvents)
	fleetWatcher := NewWatcher(framework.AgonesClient, ns, webhookReceiver.URL)
	fleetWatcher.Start(context.Background())

	gstOld := newGameServerTemplateSpec("rev1-gs", imageOld)
	gstOld.SetLabels(map[string]string{
		allocateRevisionLabel: "rev1",
	})
	newFleet(t, "rev1-fleet", ns, 0, gstOld)
	newBufferFleetAutoscaler(t, ns, "rev1-fleet", 2, 5, 2)
	ev := <-deployEvents
	assert.Equal(t, FleetDeployEventTypeStart, ev.EventType)
	ev = <-deployEvents
	assert.Equal(t, FleetDeployEventTypeFinish, ev.EventType)
	newFlt := ev.Fleet
	assert.Equal(t, "rev1", extractRevisionLabel(&newFlt))
	revisionPromoted <- "rev1"
	time.Sleep(3 * time.Second)

	log.Printf("promoting rev2...")
	gstNew := newGameServerTemplateSpec("rev2-gs", imageNew)
	gstNew.SetLabels(map[string]string{
		allocateRevisionLabel: "rev2",
	})
	newFleet(t, "rev2-fleet", ns, 0, gstNew)
	newBufferFleetAutoscaler(t, ns, "rev2-fleet", 2, 5, 2)
	ev = <-deployEvents
	assert.Equal(t, FleetDeployEventTypeStart, ev.EventType)
	ev = <-deployEvents
	assert.Equal(t, FleetDeployEventTypeFinish, ev.EventType)
	newFlt = ev.Fleet
	assert.Equal(t, "rev2", extractRevisionLabel(&newFlt))
	revisionPromoted <- "rev2"
	time.Sleep(3 * time.Second)
}

func newBufferFleetAutoscaler(t *testing.T, namespace, fleetName string, min, max, buffer int) {
	fasName := fmt.Sprintf("%s-autoscaler", fleetName)
	newFleetAutoscaler(t, namespace, fasName, &autoscalingv1.FleetAutoscalerSpec{
		FleetName: fleetName,
		Policy: autoscalingv1.FleetAutoscalerPolicy{
			Type: autoscalingv1.BufferPolicyType,
			Buffer: &autoscalingv1.BufferPolicy{
				MinReplicas: int32(min),
				MaxReplicas: int32(max),
				BufferSize:  intstr.IntOrString{Type: intstr.Int, IntVal: int32(buffer)},
			},
		},
	})
}

func logEvents(namespace string) {
	events := framework.KubeClient.EventsV1().Events(namespace)
	watch, err := events.Watch(context.Background(), metav1.ListOptions{})
	if err != nil {
		return
	}
	for {
		select {
		case obj := <-watch.ResultChan():
			ev := obj.Object.(*eventsv1.Event)
			log.Printf("+++ [event] %s", ev.Note)
		}
	}
}

func allocateLoop(t *testing.T, namespace string, revisionPromoted <-chan string) {
	for {
		select {
		case revision := <-revisionPromoted:
			log.Printf("revision promoted: %s", revision)
			allocateAndDelete(t, namespace, revision)
		}
	}
}

func allocateAndDelete(t *testing.T, namespace, revision string) {
	if revision == "" {
		return
	}
	allocated := allocate(t, namespace, &allocationv1.GameServerAllocationSpec{
		Selectors: []allocationv1.GameServerSelector{{
			LabelSelector: metav1.LabelSelector{MatchLabels: map[string]string{
				allocateRevisionLabel: revision,
			}},
		}},
	})
	if allocated.Status.State != allocationv1.GameServerAllocationAllocated {
		log.Printf("unallocated: (state: %s)", allocated.Status.State)
		return
	}
	log.Printf("allocated: (name: %s)", allocated.Status.GameServerName)
	deleteGameServer(t, namespace, allocated.Status.GameServerName)
}

func startWebhookReceiver(fleetEvents chan<- *FleetDeployEventPayload) *httptest.Server {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
		var payload FleetDeployEventPayload
		if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
			log.Printf("failed to decode %T: %+v", payload, err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		log.Printf("[webhook] received: %s %s (ready: %d/%d)", payload.EventType, payload.FleetName, payload.Fleet.Status.ReadyReplicas, payload.Fleet.Spec.Replicas)
		fleetEvents <- &payload
	}))
	return ts
}

func extractRevisionLabel(flt *agonesv1.Fleet) string {
	labels := flt.Spec.Template.GetLabels()
	if labels == nil {
		return ""
	}
	if v, ok := labels[allocateRevisionLabel]; ok {
		return v
	}
	return ""
}

func allocate(t *testing.T, namespace string, spec *allocationv1.GameServerAllocationSpec) *allocationv1.GameServerAllocation {
	gsa := &allocationv1.GameServerAllocation{
		Spec: *spec,
	}
	gsa, err := framework.AgonesClient.AllocationV1().
		GameServerAllocations(namespace).
		Create(context.Background(), gsa, metav1.CreateOptions{})
	if err != nil {
		t.Fatalf("failed to allocate: %+v", err)
	}
	return gsa
}

func deleteGameServer(t *testing.T, namespace, name string) {
	if err := framework.AgonesClient.AgonesV1().GameServers(namespace).
		Delete(context.Background(), name, metav1.DeleteOptions{}); err != nil {
		t.Fatalf("failed to delete gameserver: %+v", err)
	}
}

func watchGSS(ctx context.Context) {
	factory := externalversions.NewSharedInformerFactoryWithOptions(framework.AgonesClient, defaultResync)
	informer := factory.Agones().V1().GameServerSets()
	informer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			gss := obj.(*agonesv1.GameServerSet)
			log.Printf("GSS added: %s %d", gss.Name, gss.Spec.Replicas)
		},
		UpdateFunc: func(oldObj, newObj interface{}) {
			oldGss := oldObj.(*agonesv1.GameServerSet)
			gss := newObj.(*agonesv1.GameServerSet)
			log.Printf("GSS updated: %s %d -> %d", gss.Name, oldGss.Spec.Replicas, gss.Spec.Replicas)
		},
		DeleteFunc: func(obj interface{}) {
			gss := obj.(*agonesv1.GameServerSet)
			log.Printf("GSS deleted: %s %d", gss.Name, gss.Spec.Replicas)
		},
	})
	factory.Start(ctx.Done())
}
